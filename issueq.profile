<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * Alter the install profile settings form.
 */
function system_form_install_settings_form_alter(&$form, &$form_state) {
  $form['settings']['mysql']['advanced_options']['db_prefix']['#default_value'] = 'iq7_';
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function issueq_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = 'IssueQ';
  $form['site_information']['site_mail']['#default_value'] = 'noreply@kybest.hu';
  $form['admin_account']['account']['name']['#default_value'] = 'issueq-admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'webmaster@kybest.hu';
}

/**
 * Implements hook_install_tasks_alter().
 *
 * @param $tasks
 *   An array of all available installation tasks.
 * @param $install_state
 *   An array of information about the current installation state.
 *
 * Unset unnecessary default language tasks.
 */
function issueq_install_tasks_alter(&$tasks, &$install_state) {
  // Modify install select locale task.
  $tasks['install_select_locale']['display_name'] = st('Select languages');
  $tasks['install_select_locale']['function'] = 'issueq_install_select_languages';

  // Remove core steps for translation imports.
  unset($tasks['install_import_locales']);
  unset($tasks['install_import_locales_remaining']);
}

/**
 * Custom callback function for the default install_select_locale task.
 *
 * @param $install_state
 *   An array of information about the current installation state.
 *
 * @return bool|string
 *   - Returns "nothing" if locale parameter is submitted with POST action.
 *   - Returns the rendered language selection form if locale parameter is
 *   missing in the URL.
 *
 * @throws Exception
 *   Throws exception if the selected default language is not English.
 *
 * Note: This is a modification of the default install_select_locale task.
 * Search for documentations about this function for more details.
 */
function issueq_install_select_languages(&$install_state) {
  // Define selectable languages.
  $selectable_languages = array(
    'da' => 'Danish (Dansk)',
    'fi' => 'Finnish (Suomi)',
    'fr' => 'French (Français)',
    'de' => 'German (Deutsch)',
    'hu' => 'Hungarian (Magyar)',
    'it' => 'Italian (Italiano)',
    'ro' => 'Romanian (Română)',
    'sk' => 'Slovak (Slovenčina)',
    'sv' => 'Swedish (Svenska)',
  );

  // If $_POST['locale'] is available, then add it to the URL as a parameter.
  // Drupal tasks need it later.
  if (!empty($_POST['locale'])) {
    if ($_POST['locale'] == 'en') {
      $install_state['parameters']['locale'] = $_POST['locale'];
    }
    else {
      throw new Exception(st('English can be the only default language.'));
    }

    // If $_POST['additional_languages'] available, then add it to the URL as a
    // parameter. We will need it later when we want to import translations.
    // With this method, we can simply retrieve the previously selected language
    // codes later from the URL, and download the necessary translations when
    // database connection is available.
    if (!empty($_POST['additional_languages'])) {
      $additional_languages = '';
      foreach ($_POST['additional_languages'] as $langcode => $selected) {
        if (array_key_exists($langcode, $selectable_languages)) {
          if ($selected) {
            $additional_languages .= ($additional_languages == '') ? $langcode : '+' . $langcode;
          }
        }
        else {
          throw new Exception(st('Invalid language code.'));
        }
      }
      $install_state['parameters']['additional_languages'] = $additional_languages;
    }
    return;
  }
  else {
    if (empty($install_state['parameters']['locale'])) {
      // If locale parameter is missing in the URL, then we return
      // with the "select languages" form.
      drupal_set_title(st('Select languages what you would like to install'));

      include_once DRUPAL_ROOT . '/includes/form.inc';
      $select_languages_form = drupal_get_form('issueq_install_select_languages_form', $selectable_languages);
      return drupal_render($select_languages_form);
    }
  }
}

/**
 * Custom language selection form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 * @param $additional_languages
 *   An array containing selectable additional languages.
 *
 * @return mixed
 *   Returns the modified language selection form.
 */
function issueq_install_select_languages_form($form, &$form_state, $additional_languages) {
  $default_languages = array('en' => 'English (built-in)');

  $form['locale'] = array(
    '#type' => 'radios',
    '#title' => st('Default language'),
    '#options' => $default_languages,
    '#default_value' => 'en',
  );

  $form['additional_languages'] = array(
    '#type' => 'checkboxes',
    '#title' => st('Additional languages'),
    '#options' => $additional_languages,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
  );

  return $form;
}

/**
 * Implement hook_install_tasks().
 *
 * @param $install_state
 *   An array of information about the current installation state.
 *
 * @return array
 *   An array of custom install tasks.
 */
function issueq_install_tasks($install_state) {
  return array(
    'issueq_install_basic_features' => array(
      'display_name' => st('Install basic features'),
      'display' => TRUE,
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'type' => 'batch',
    ),
    'issueq_install_select_extra_features_form' => array(
      'display_name' => st('Select extra features'),
      'display' => TRUE,
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'type' => 'form',
    ),
    'issueq_install_extra_features' => array(
      'display_name' => st('Install extra features'),
      'display' => TRUE,
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'type' => 'batch',
    ),
    'issueq_install_import_translations' => array(
      'display_name' => st('Set up translations'),
      'display' => TRUE,
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'type' => 'batch',
    ),
    'issueq_install_rebuild_content_access' => array(
      'display_name' => st('Rebuild content access'),
      'display' => TRUE,
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'type' => 'normal',
    ),
  );
}

/**
 * Installation step callback to enable basic features.
 *
 * @param $install_state
 *   An array of information about the current installation state.
 *
 * @return array
 *   An array of batch operations.
 */
function issueq_install_basic_features(&$install_state) {
  // Some of features may start as overridden, eg. in the case they have
  // strongarmed variables, because the accompanying module which gets
  // installed as a dependency of the feature sets a variable in its
  // hook_install(), so the feature's variable becomes overridden by it. To
  // get around this, we are installing the feature modules' dependencies as
  // dependencies of the install profile itself (so the dependencies are
  // installed properly, eg. with their translation being downloaded and
  // installed), and as the final installation step we simply install the
  // feature modules and immediately revert all the components of all them.
  // Weird, but yields the expected results.
  $batch = array(
    'title' => st('Enabling and configuring feature modules'),
    'operations' => array(),
  );

  $modules = array(
    'iq_settings',
    'iq_cts',
    'iq_documentation',
    'iq_issue',
    'iq_news',
    'iq_notification_rules',
    'iq_project',
    'iq_flags',
  );
  foreach ($modules as $module) {
    $batch['operations'][] = array(
      'features_install_modules',
      array(array($module))
    );
  }
  $batch['operations'][] = array('features_revert', array());

  return $batch;
}

/**
 * Custom form builder for selecting extra features to install.
 */
function issueq_install_select_extra_features_form($form, &$form_state, &$install_state) {
  drupal_set_title('Select extra features to install beside the issue tracker.');
  $form = array();
  $form['kanban_development_tools'] = array(
    '#type' => 'checkbox',
    '#title' => st('Kanban Development Tools'),
    '#description' => st('!Kanban is a scheduling system for lean and just-in-time (JIT) production.', array('!Kanban' => l('Kanban', 'http://en.wikipedia.org/wiki/Kanban'))),
  );
  $form['search_function'] = array(
    '#type' => 'checkbox',
    '#title' => st('Search function'),
    '#description' => st('Users can search inside comments and contents at the same time with a !FacetApi like filtered interface.', array('!FacetApi' => l('Facet API', 'https://drupal.org/project/facetapi'))),
  );
  $form['test_users'] = array(
    '#type' => 'checkbox',
    '#title' => st('Add test users'),
    '#description' => st('Adding test users may help demonstrating IssueQ features and kickstart debugging and development.'),
  );
  $form['issueq_debug'] = array(
    '#type' => 'checkbox',
    '#title' => st('IssueQ debug'),
    '#description' => st('Enable debug cruft to make IssueQ development easier. Some highlights: Devel, Views UI, Field UI, Contextual links, Mail System modules; User switcher block; etc.'),
  );
  $form['issueq_bugreport'] = array(
    '#type' => 'checkbox',
    '#title' => st('Bug reporting'),
    '#description' => st('Enable block and form for reporting bugs.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
  );
  return $form;
}

/**
 * Install step callback to install selected extra features.
 *
 * @param $install_state
 *   An array of information about the current installation state.
 *
 * @return array
 *   An array of batch operations.
 */
function issueq_install_extra_features() {
  $batch = array(
    'title' => st('Enabling and configuring extra features'),
    'operations' => array(),
  );

  // Install Kanban Developement Tools if selected.
  if (isset($_POST['kanban_development_tools']) && $_POST['kanban_development_tools']) {
    $features = array(
      'iq_kanban_board',
    );
    foreach ($features as $feature) {
      $batch['operations'][] = array(
        'features_install_modules',
        array(array($feature))
      );
    }
    // Features needs to be reverted.
    $batch['operations'][] = array('features_revert', array());
  }

  // Install Search function if selected.
  if (isset($_POST['search_function']) && $_POST['search_function']) {
    $features = array(
      'iq_search',
    );
    foreach ($features as $feature) {
      $batch['operations'][] = array(
        'features_install_modules',
        array(array($feature))
      );
    }
    // Features needs to be reverted.
    $batch['operations'][] = array('features_revert', array());
  }

  // Add test users if selected.
  if (isset($_POST['test_users']) && $_POST['test_users']) {
    $users = array(
      'developer-hu-1',
      'developer-hu-2',
      'developer-en-1',
      'developer-en-2',
      'customer-hu-1',
      'customer-en-1',
      'pmanager-hu-1',
      'pmanager-en-1',
    );

    foreach ($users as $user) {
      $fields = array(
        'name' => $user,
        'mail' => $user . '@kybest.hu',
        'pass' => 'teszt',
        'status' => 1,
        // FIXME: Only create non-English users if the appropriate language has
        // FIXME: been selected beforehand.
        'language' => strrpos($user, 'en') !== FALSE ? 'en' : 'hu',
        'roles' => array(
          DRUPAL_AUTHENTICATED_RID => 'authenticated user',
        ),
      );
      if (strrpos($user, 'pmanager') !== FALSE) {
        $fields['roles'][] = 'admin';
      }
      user_save('', $fields);
    }
  }

  // Enable debug cruft if selected.
  if (isset($_POST['issueq_debug']) && $_POST['issueq_debug']) {
    // Catch all mails.
    variable_set('mail_system', array('default-system' => 'DevelMailLog'));

    // Enable Switch user block in first sidebar.
    db_insert('block')
      ->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'cache'))
      ->values(array(
        'module' => 'devel',
        'delta' => 'switch_user',
        'theme' => variable_get('theme_default', 'iq_garland'),
        'status' => BLOCK_CUSTOM_ENABLED,
        'weight' => 3,
        'region' => 'sidebar_first',
        'pages' => '',
        'cache' => DRUPAL_NO_CACHE,
      ))
      ->execute();

    // Enable development modules.
    $modules = array(
      'devel',
      'views_ui',
      'field_ui',
      'contextual',
      'mailsystem',
    );
    foreach ($modules as $module) {
      $batch['operations'][] = array(
        'module_enable',
        array(array($module))
      );
    }
    // Reverting features should not harm.
    $batch['operations'][] = array('features_revert', array());
  }

  // Enable bug reporting if selected.
  if (isset($_POST['issueq_bugreport']) && $_POST['issueq_bugreport']) {
    // Enable the block first.
    $bugreport_block = array(
      'module' => 'iq_bugreport',
      'delta' => 'bugreport',
      'theme' => variable_get('theme_default', 'bartik'),
      'status' => BLOCK_CUSTOM_ENABLED,
      'weight' => -1,
      'region' => 'sidebar_first',
      'pages' => '',
      'cache' => DRUPAL_NO_CACHE,
    );
    $query = db_insert('block')
      ->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'cache'))
      ->values($bugreport_block)
      ->execute();
    // Then fire up the batch to enable the feature module itself.
    $features = array(
      'iq_bugreport',
    );
    foreach ($features as $feature) {
      $batch['operations'][] = array(
        'features_install_modules',
        array(array($feature))
      );
    }
    // Features needs to be reverted.
    $batch['operations'][] = array('features_revert', array());
  }

  return $batch;
}

/**
 * Installation step callback to import translations.
 *
 * @param $install_state
 *   An array of information about the current installation state.
 *
 * @return array
 *   An array of batch operations.
 */
function issueq_install_import_translations(&$install_state) {
  // If previously selected language codes available in the URL,
  // then we need to download the translations.
  if (!empty($install_state['parameters']['additional_languages'])) {
    // It is important to enable l10n_update module (and its dependencies)
    // here instead of include it under the profile dependencies, and
    // before the locale_add_language() method called, because the latter
    // depends on locale module.
    module_enable(array('l10n_update'));

    include_once DRUPAL_ROOT . '/includes/locale.inc';

    // Get selected language codes.
    $additional_languages = explode('+', $install_state['parameters']['additional_languages']);

    // Add selected languages.
    foreach ($additional_languages as $langcode) {
      locale_add_language($langcode, NULL, NULL, NULL, '', NULL, TRUE, FALSE);
    }

    // Build batch with l10n_update module to download translations.
    // l10n_update will download (core and module) translations in each
    // language which are added earlier by locale_add_language() method.
    module_load_include('batch.inc', 'l10n_update');
    $history = l10n_update_get_history();
    $available = l10n_update_available_releases();
    $updates = l10n_update_build_updates($history, $available);
    $updates = _l10n_update_prepare_updates($updates, NULL, array());
    $batch = l10n_update_batch_multiple($updates, LOCALE_IMPORT_OVERWRITE);

    return $batch;
  }
  else {
    // If language codes aren't available, then create an empty batch.
    $batch = array(
      'title' => t('There is no need to import translations'),
      'operations' => array(),
    );

    return $batch;
  }
}

/**
 * Installation step callback.
 *
 * @param $install_state
 *   An array of information about the current installation state.
 *
 * Organic groups access control requires to rebuild the node access.
 */
function issueq_install_rebuild_content_access(&$install_state) {
  // node_access_rebuild(TRUE); doesn't work. Weird...
  features_revert();
  node_access_rebuild();
}
